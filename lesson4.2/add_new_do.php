<?php
session_start();
if(empty($_SESSION['id'])) {
    header("Location: /");
}
?>
<html>
<head>
    <title>Добавление нового дела</title>
</head>
<body>
<h2>Добавление нового дела</h2>
<form method="post" action="add_new_do.php">
<p>
    <label>Дело:<br></label>
    <input type="text" name="delo">
</p>
    <p>
        <input type="submit" name="submit" value="Добавить дело">
    </p>
</form>
</body>
</html>
<?php
$id=$_SESSION['id'];
if(isset($_POST['delo'])){
    $delo=$_POST['delo'];
    if($delo==''){
        unset($delo);
    }
}

$delo=stripslashes($delo);
$delo=htmlspecialchars($delo);
$delo=trim($delo);
if(empty($delo)){
    exit("Ошибка. Введите описание.");
}
include "bd.php";
$sql = "INSERT INTO task (user_id,assigned_user_id,description,date_added ) VALUE('$id','$id','$delo',NOW())";
$add = $pdo->query($sql);
if ($add == true) {
    echo "Вы успешно добавили задачу. Теперь вы можете просмотреть список задач. <a href='spisok_del.php'>Список задач</a>";
} else {
    echo "Ошибка добавления.";
}
?>
