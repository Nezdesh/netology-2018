<?php
session_start();
if(empty($_SESSION['id'])) {
    header("Location: /");
}
$id=$_SESSION['id'];
include "bd.php";
$sql = "SELECT description,date_added,is_done,assigned_user_id,id FROM task  WHERE user_id='$id' ORDER BY date_added";
$tab=$pdo->query($sql);
$tab_1=$tab->fetchAll(PDO::FETCH_ASSOC);
$ql = "SELECT login,id FROM user";
$spisok=$pdo->query($ql);
$alluser=$spisok->fetchAll(PDO::FETCH_ASSOC);

?>
<html>
<head>
    <title>Список дел</title>
</head>
<body>
<h2>Список дел</h2>
<form method="post">
<table border="1">
    <tbody>
    <tr>
        <th>Описание</th>
        <th>Дата</th>
        <th>Статус</th>
        <th>Исполнитель</th>
        <th>Действие</th>
    </tr>
    <?php
    $i=0;
    foreach ($tab_1 as $value){?>
    <tr>
        <?php
        foreach ($value as$key=> $v){?>
        <td align="center"> <?php
            if($key=='id'){?>
                <button type="submit" formaction="delete_do.php" name="id" value="<?php echo $v;?>">Удалить</button>
           <?php
            }else{
                if($key=='is_done'){
                    if($v=='0'){
                        $status="Не выполнено";
                    }else{
                        $status="Выполнено";
                    }?>
                    <button type="submit" formaction="status.php" name="id" value="<?php echo $value['id'];?>"><?php echo$status;?></button>
              <?php

                }else{
            if ($key=='assigned_user_id'){
                if($v==$id){?>
            <p>
                <input name="id" type="hidden" value="<?php echo $value['id'];?>">
                <select  name="delegate_id">
                    <option ><?php echo $_SESSION['login']; ?></option>
                    <?php foreach ($alluser as $User){
                        if($User['login']!=$_SESSION['login']){?>
                            <option value=" <?php echo $User['id']; ?>"> <?php echo $User['login'];?> </option>
                        <?php }} ?>
                </select>
                <input type="submit" formaction="delegate.php" value="Делегировать"></p>
            <?php
                }else{
                    foreach ($alluser as $User){
                        if($User['id']==$v){
                            echo$User['login'];
                        }
                    }
                }
            }else{
            echo"$v";}}}?>
        </td>
       <?php } ?>
    </tr>
    <?php } ?>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td> <input value="Добавить" type="button" onclick="location.href='./add_new_do.php'" /></td>
    </tr>
    <tr>
        <td><input value="Список делегированного" type="button" onclick="location.href='./spisok_delegir.php'" />  </td>
        <td align="center"><input value="Полный список дел" type="button" onclick="location.href='./spisok_All_Do.php'" />  </td>
    </tr>
    </tbody>
</form>
</table>
<p>
    <a href='logout.php'>Выход</a>
</p>
</body>
</html>
