<?php
session_start();
if ($_SESSION['status'] == 'admin') {
    http_response_code(403);
    exit();
} else {
    ?>
    <html>
    <head>
        <title>Тест</title>
        <meta charset="UTF-8">
    </head>
    <body>
    <?php
    if (isset($_POST['vopros1'])) {
        $sertif['name'] = $_SERVER['PHP_AUTH_USER'];
        foreach ($_SESSION['otv'] as $p => $_otv) {
            $w++;
            if ($_otv == $_POST["vopros$w"]) {
                $sertif[] = " Вы правльно ответили на вопрос " . "$w";
            } else {
                $sertif[] = " Вы не правильно ответили на вопрос " . "$w";
            }
        }
        $_SESSION['sertif'] = $sertif;
        header("HTTP/1.1 301 Moved Permanently");
        header('Refresh:0.01 ;  ./jpg.php');
        exit();

//print_r($_POST);
//print_r($_SESSION['otv']);

    } else {
        if (isset($_GET['nomer']) and is_numeric($_GET['nomer'])) {
//var_dump($_GET);
            $t = file_get_contents("test" . "{$_GET['nomer']}" . ".json");
            if ($t === false) {
                http_response_code(404);
                echo 'Cтраницане найдена!';
                exit();
            }
            $d = json_decode($t, true);
            ?>
            <form action="test.php" method="POST">
                <?php
                $i = 0;
                // $_SESSION['otv']=array();
                foreach ($d as $key => $vopros) {
                    ?>
                    <div><p> <?php
                    if (strpos($key, "vopros") !== false) {
                        $i++;
                        $u = 1;
                        $_SESSION['otv']["otv$i"] = $d["otvet$i"];
                        echo $vopros; ?></p></div>
                    <?php } else {
                        if (strpos($key, 'var') !== false) { ?>
                            <label><input name="<?php echo "vopros$i"; ?>" type="radio"
                                          value="<?php echo $u++; ?>"><?php echo $vopros; ?></label>
                        <?php }
                    }
                }
                ?>
                <button type="submit">Результаты</button>
            </form>
            <?php
        } else {
            ?>
            <h1>Выбор теста</h1>
            <form method="get" action="test.php">
                <input type="number" name="nomer">
                <input type="submit" name="Knopka" value="Выбор">
            </form>
        <?php }
    }
    ?>
    </body>
    </html>
    <?php
}
?>
