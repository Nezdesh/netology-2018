<?php
session_start();
if (!isset($_SERVER['PHP_AUTH_USER'])) {
    http_response_code(403);
    exit();
} else {
    ?>
    <html>
    <head>
        <title>Список загруженных файлов</title>
        <meta charset="UTF-8">
    </head>
    <body>
    <?php
    $b[] = 'Список';
    foreach (glob('test*.json') as $v) {
        $b[] = $v;
    }
    $_SESSION['tests'] = $b;
    ?>
    <h1>Список загруженных тестов</h1>
    <table border="1">
        <?php foreach ($b as $key => $value) { ?>
            <tr>
                <td> <?php if ($key != 0) {
                        echo $key;
                    } else {
                        echo "№";
                    } ?></td>
                <td> <?php echo $value; ?></td>
                <?php if ($_SESSION['status'] == 'admin') { ?>
                    <td><?php if ($key == 0) {
                        echo 'удаление';
                    } else { ?>
                        <form method="post">
                            <button type="submit" name="удалить" value="<?php echo $value; ?>"> удалить</button>
                        </form></td> <?php }
                } ?>
            </tr>
        <?php } ?>
    </table>
    <?php
    if ($_SESSION['status'] == 'admin') {
        if (isset($_POST['удалить'])) {
            unlink($_POST['удалить']);
            $_POST['удалить'] = null;
            header("Refresh: 0");
        }
        ?>

        <form>
            <input type='button' value='добавить тест' onclick="location='./admin.php'">
        </form>
        <?php
    }
    ?>
    </body>
    </html>
<?php }
?>
