<?php
$servername = "127.0.0.1"; // локалхост
$port = '8889';
$username = "root"; // имя пользователя
$password = "root"; // пароль если существует
$dbname = "myBD"; // база данных

try {
    $conn = new PDO("mysql:host=$servername;port=$port;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Подключение не удалось: ' . $e->getMessage();
}
