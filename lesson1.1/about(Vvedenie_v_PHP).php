<?php
$name = 'Алексей';
$age = 28;
$email = 'Sivenkovaleksei@mail.ru';
$city = 'Апрелевка';
$about = 'Конструктор';
?>
<html lang="ru">
<head>
    <title>
        <?= $name . '_' . $about ?></title>
    <meta charset="utf-8">
</head>
<body>
<h1>Cтраница пользователя <?= $name ?></h1>
<dl>
    <dt>Имя</dt>
    <dd><?= $name ?></dd>
    <dt>Возраст</dt>
    <dd><?= $age ?></dd>
    <dt>Адрес электронной почты</dt>
    <dd><a href="mailto:<?= $email ?>"><?= $email ?></a></dd>
    <dt>Город</dt>
    <dd><?= $city ?></dd>
    <dt>О себе</dt>
    <dd><?= $about ?></dd>
</dl>
</body>
</html>


