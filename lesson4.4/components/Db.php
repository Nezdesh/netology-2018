<?php


class Db
{
    public static function getConnection()
    {
        $paramsPath = ROUD . '/config/db_params.php';
        $params = include($paramsPath);
        $db = new PDO("mysql:host={$params['servername']};port={$params['port']};dbname={$params['dbname']}", $params['user'], $params['password']);
        return $db;
    }
}