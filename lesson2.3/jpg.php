<?php
session_start();

foreach ($_SESSION['sertif'] as $res) {
    $c .= "$res\n";
}
$code = 'Результат теста' . ' ' . $c;
$image = imagecreatetruecolor(1042, 607);
$bekcolor = imagecolorallocate($image, 1, random_int(1, 255), random_int(1, 255));
$textcolor = imagecolorallocate($image, random_int(1, 255), random_int(1, 255), random_int(1, 255));
$boxfile = __DIR__ . '/upr.png';
if (!file_exists($boxfile)) {
    echo "файл изображения не найден";
    echo __DIR__ . '/upr.png';
    exit();
}
$imbox = imagecreatefrompng($boxfile);
imagefill($image, 0, 0, $bekcolor);
imagecopy($image, $imbox, 10, 20, 0, 0, 1042, 607);
$text = __DIR__ . '/19.otf';
if (!file_exists($text)) {
    echo "не найден шрифт";
    exit();
}
imagettftext($image, 30, random_int(0, 0), 200, 200, $textcolor, $text, $code);
header('Content-Type: image/png');
imagepng($image);