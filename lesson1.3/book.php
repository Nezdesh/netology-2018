<html>
<head>

    <title>books</title>
    <meta charset="UTF-8">
</head>
<body>
<?php
if ($argv !== null and $argv[1] !== null) {
    $zapros = file_get_contents('https://www.googleapis.com/books/v1/volumes?q={' . urlencode($argv[1]) . '}') or die("ошибка запроса");
    $b = json_decode($zapros, true);
    $error = json_last_error();
    print_r($b);
//print_r($b['items']['0']['id']); адресс ид
//print_r($b['items']['0']['volumeInfo']['title']); адресс заголоака
//print_r($b['items']["$key"]['volumeInfo']['authors']['0']); адресс автор
    $csv = fopen('books.csv', "a");
    foreach ($b['items'] as $key => $value) {
        $book = [$b['items']["$key"]['id'], $b['items']["$key"]['volumeInfo']['title'], $b['items']["$key"]['volumeInfo']['authors']['0']];
        fputcsv($csv, $book);
    }
    fclose($csv);
} else {
    echo "пустой запрос";
}
?>
</body>
</html>