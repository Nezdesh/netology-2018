<?php


class News
{
    public static function getNewsItemById($id)
    {
        $id = intval($id);
        if ($id) {
            $db = Db::getConnection();

            $result = $db->query('SELECT * FROM news WHERE id=' . $id);
            $newsItem = $result->fetch(PDO::FETCH_ASSOC);

            return $newsItem;
        }
    }


    public static function getNewsList()
    {
        $db = Db::getConnection();

        $newsList = array();

        $result = $db->query('SELECT id,author_name,title,date,short_content FROM news ORDER BY date DESC LIMIT 10');
        $i = 0;
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $newsList[$i]['id'] = $row['id'];
            $newsList[$i]['author_name'] = $row['author_name'];
            $newsList[$i]['title'] = $row['title'];
            $newsList[$i]['date'] = $row['date'];
            $newsList[$i]['short_content'] = $row['short_content'];
            $i++;


        }
        return $newsList;
    }

}