<?php


class Router
{
//переменная для маршрутов
    private $routes;

    public function __construct()
    {
        $routerPath = ROOT . '/config/routes.php';
        $this->routes = include($routerPath);

    }

    /**
     * Return request string
     */
    //запрос чего там прилелетело в урле
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

//старт роутра
    public function run()
    {
        $uri = $this->getURI();

//для каждого маршрута ищем совпадение ключа с данными из урла
        foreach ($this->routes as $uriPattern => $path) {
            if (preg_match("~$uriPattern~", $uri)) {
                //получаем внутренний путь
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                $segments = explode('/', $internalRoute);
//формируем имена контролера и экшена из кусков строки совпавшего маршрута( нифига не понимаю почему не через фор ич??????)
                $controllerName = ucfirst(array_shift($segments)) . 'Controller';

                $actionName = 'action' . ucfirst(array_shift($segments));

//получаем параметры
                $parametrs = $segments;
                //print_r($parametrs);


                $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';
                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                }
                $controllerObject = new $controllerName;
                $result = call_user_func_array(array($controllerObject, $actionName), $parametrs);
                //$controllerObject->$actionName($parametrs); возможный вариант (нужно еще $parametrs вписать нужный контроллер как функция

                if ($result != null) {
                    break;
                }


            }


        }

    }


}